package com.springwell.service;

import com.springwell.entity.Customer;
import com.springwell.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer findById(Long Id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(Id);
        return optionalCustomer.orElse(new Customer(0L, "Default", "Default", "customer.service@springwell.co.uk"));
    }

    public List<Customer> findAll() {
        Iterable<Customer> customerIterable = customerRepository.findAll();
        List<Customer> customers = new ArrayList<>();
        customerIterable.forEach(customers::add);
        return customers;
    }
}
