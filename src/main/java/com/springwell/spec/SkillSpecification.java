package com.springwell.spec;

import com.springwell.entity.Employee;
import com.springwell.entity.SkillSet;
import com.springwell.entity.Specification;

public record SkillSpecification(SkillSet skillSet) implements Specification<Employee> {

    @Override
    public boolean isSatisfied(Employee item) {
        return this.skillSet == item.getSkillSet();
    }
}