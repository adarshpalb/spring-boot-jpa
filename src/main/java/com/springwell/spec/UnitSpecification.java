package com.springwell.spec;

import com.springwell.entity.Employee;
import com.springwell.entity.Specification;
import com.springwell.entity.Unit;

public class UnitSpecification implements Specification<Employee> {

    private final Unit unit;

    public UnitSpecification(Unit unit) {
        this.unit = unit;
    }

    @Override
    public boolean isSatisfied(Employee employee) {
        return this.unit == employee.getUnit();
    }
}
