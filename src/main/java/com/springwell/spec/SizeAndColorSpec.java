package com.springwell.spec;

import com.springwell.entity.Product;
import com.springwell.entity.Specification;

public class SizeAndColorSpec implements Specification<Product> {

    private Specification<Product> first;
    private Specification<Product> second;

    public SizeAndColorSpec(Specification<Product> first, Specification<Product> second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean isSatisfied(Product item) {
        return first.isSatisfied(item) && second.isSatisfied(item);
    }
}
