package com.springwell.spec;

import com.springwell.entity.Color;
import com.springwell.entity.Product;
import com.springwell.entity.Specification;

public class ColorSpecification implements Specification<Product> {

    private final Color color;

    public ColorSpecification(Color color) {
        this.color = color;
    }

    @Override
    public boolean isSatisfied(Product item) {
        return this.color == item.getColor();
    }
}
