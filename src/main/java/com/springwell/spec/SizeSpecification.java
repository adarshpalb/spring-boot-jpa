package com.springwell.spec;

import com.springwell.entity.Product;
import com.springwell.entity.Size;
import com.springwell.entity.Specification;

public class SizeSpecification implements Specification<Product> {

    private final Size size;

    public SizeSpecification(Size size) {
        this.size = size;
    }

    @Override
    public boolean isSatisfied(Product item) {
        return this.size == item.getSize();
    }
}
