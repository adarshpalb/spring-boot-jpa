package com.springwell.spec;

import com.springwell.entity.Product;
import com.springwell.entity.ProductType;
import com.springwell.entity.Specification;

public class ProductTypeSpecification implements Specification<Product> {

    private ProductType productType;

    public ProductTypeSpecification(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public boolean isSatisfied(Product item) {
        return this.productType == item.getProductType();
    }
}
