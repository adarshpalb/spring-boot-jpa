package com.springwell.res;

import com.springwell.entity.Customer;
import com.springwell.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerResource {

    private final CustomerService customerService;

    @Autowired
    public CustomerResource(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers/{Id}" )
    public Customer getCustomer(@PathVariable Long Id) {
        return customerService.findById(Id);
    }

    @GetMapping("/customers" )
    public List<Customer> getAllCustomers() {
        return customerService.findAll();
    }
}