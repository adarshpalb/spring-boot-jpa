package com.springwell.repo;

import com.springwell.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findByLastNameOrderByFirstNameAsc(String lastName);

    List<Customer> findByLastName(String firstName);

}