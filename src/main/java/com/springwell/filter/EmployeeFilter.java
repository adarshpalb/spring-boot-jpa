package com.springwell.filter;

import com.springwell.entity.Employee;
import com.springwell.entity.SkillSet;
import com.springwell.entity.Specification;
import com.springwell.entity.Unit;

import java.util.List;

public class EmployeeFilter implements Filter<Employee> {

    @Override
    public List<Employee> filter(List<Employee> items, Specification<Employee> specification) {
        return items.stream()
                .filter(specification::isSatisfied)
                .toList();
    }
}