package com.springwell.filter;

import com.springwell.entity.Specification;

import java.util.List;

public interface Filter<T> {

    List<T> filter(List<T> items, Specification<T> specification);
}
