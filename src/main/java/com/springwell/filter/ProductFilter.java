package com.springwell.filter;

import com.springwell.entity.Product;
import com.springwell.entity.Specification;

import java.util.List;

public class ProductFilter implements Filter<Product> {

    @Override
    public List<Product> filter(List<Product> items, Specification<Product> specification) {
        return items.stream().filter(specification::isSatisfied).toList();
    }
}
