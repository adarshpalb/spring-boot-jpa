package com.springwell.pattern;

import net.bytebuddy.implementation.bytecode.Throw;

public class InterfaceSegregation {

    public static void main(String[] args) {

    }
}

class Document {

}

interface Machine {

    void print(Document document);
    void scan(Document document) throws Exception;
    void copy(Document document);
    void fax(Document document);

}


class OldFashionedPrinter implements Machine {

    @Override
    public void print(Document document) {
        System.out.println("Printing Document");
    }

    @Override
    public void scan(Document document) throws Exception {
        throw new Exception("Printer cannot scan");
    }

    @Override
    public void copy(Document document) {

    }

    @Override
    public void fax(Document document) {

    }
}
