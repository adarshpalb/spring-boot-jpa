package com.springwell.entity;

public enum SkillSet {

    JAVA, DBMS, SCALA, CLOUD, SAP, GENERIC, HR
}
