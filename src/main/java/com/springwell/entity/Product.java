package com.springwell.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigDecimal;

public class Product {

    private Long productID;
    private String productMake;
    private String productName;
    private ProductType productType;
    private BigDecimal productPrice;
    private Color color;
    private Size size;


    public Product(Long productID, String productMake, String productName, ProductType productType,
                   BigDecimal productPrice, Color color, Size size) {
        this.productID = productID;
        this.productMake = productMake;
        this.productName = productName;
        this.productType = productType;
        this.productPrice = productPrice;
        this.color = color;
        this.size = size;
    }

    public Long getProductID() {
        return productID;
    }

    public void setProductID(Long productID) {
        this.productID = productID;
    }

    public String getProductMake() {
        return productMake;
    }

    public void setProductMake(String productMake) {
        this.productMake = productMake;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("productID", productID)
                .append("productMake", productMake)
                .append("productName", productName)
                .append("productType", productType)
                .append("productPrice", productPrice)
                .append("color", color)
                .append("size", size)
                .toString();
    }
}
