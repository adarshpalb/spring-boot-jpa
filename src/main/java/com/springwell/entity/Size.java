package com.springwell.entity;

public enum Size {
    SMALL, MEDIUM, LARGE, XL
}
