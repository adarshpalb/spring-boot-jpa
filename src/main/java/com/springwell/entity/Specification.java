package com.springwell.entity;

public interface Specification<T> {

    boolean isSatisfied(T item);
}
