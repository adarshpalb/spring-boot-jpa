package com.springwell.entity;

public enum ProductType {

    PHONE, TABLET, LAPTOP, DESKTOP, KEYBOARD, MOUSE, WATCH, CHARGER
}
