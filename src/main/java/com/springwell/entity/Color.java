package com.springwell.entity;

public enum Color {

    RED, BLUE, GREEN, WHITE, BLACK
}
