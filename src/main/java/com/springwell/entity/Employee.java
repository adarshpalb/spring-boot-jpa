package com.springwell.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

public record Employee(Long id, String firstName, String lastName,
                       Unit unit, SkillSet skillSet) {

    public Unit getUnit() {
        return this.unit;
    }

    public SkillSet getSkillSet() {
        return this.skillSet;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("unit", unit)
                .append("skillSet", skillSet)
                .toString();
    }
}