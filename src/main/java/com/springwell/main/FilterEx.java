package com.springwell.main;

import com.springwell.entity.Employee;
import com.springwell.entity.SkillSet;
import com.springwell.entity.Unit;
import com.springwell.filter.EmployeeFilter;
import com.springwell.spec.SkillSpecification;
import com.springwell.spec.UnitSpecification;

import java.util.Arrays;
import java.util.List;

public class FilterEx {

    public static void main(String[] args) {

        Employee employee1 = new Employee(1001L, "Adarshpal", "Brar", Unit.IT, SkillSet.JAVA);
        Employee employee2 = new Employee(1002L, "Jiri", "P", Unit.IT, SkillSet.JAVA);
        Employee employee3 = new Employee(1003L, "Audira", "Fearon", Unit.HR, SkillSet.HR);
        Employee employee4 = new Employee(1004L, "Nick", "Dutton", Unit.MGR, SkillSet.GENERIC);

        List<Employee> employees = Arrays.asList(employee1, employee2, employee3, employee4);

        EmployeeFilter filter = new EmployeeFilter();

        filter.filter(employees, new SkillSpecification(SkillSet.JAVA))
                .forEach(employee -> System.out.println("Java Employees: " + employee));


        filter.filter(employees, new UnitSpecification(Unit.MGR))
                .forEach(employee -> System.out.println("Managers: " + employee));

    }
}
