package com.springwell.main;

import com.springwell.entity.*;
import com.springwell.filter.ProductFilter;
import com.springwell.spec.ColorSpecification;
import com.springwell.spec.ProductTypeSpecification;
import com.springwell.spec.SizeAndColorSpec;
import com.springwell.spec.SizeSpecification;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class ProductFilterEx {

    public static void main(String[] args) {

        Product iPhone = new Product(10001L, "Apple", "iPhone", ProductType.PHONE,
                new BigDecimal(1200), Color.WHITE, Size.LARGE);

        Product tablet = new Product(10002L, "Apple", "Tablet", ProductType.TABLET,
                new BigDecimal(1200), Color.WHITE, Size.LARGE);

        Product iWatch = new Product(10003L, "Apple", "iWatch", ProductType.WATCH,
                new BigDecimal(1200), Color.BLACK, Size.LARGE);

        Product keyboard = new Product(10004L, "Apple", "Keyboard", ProductType.KEYBOARD,
                new BigDecimal(1200), Color.WHITE, Size.LARGE);

        Product mouse = new Product(10005L, "Apple", "Mouse", ProductType.MOUSE,
                new BigDecimal(1200), Color.WHITE, Size.LARGE);

        Product galaxy = new Product(10006L, "Samsung", "Galaxy", ProductType.PHONE,
                new BigDecimal(1200), Color.WHITE, Size.LARGE);

        List<Product> productList = Arrays.asList(iPhone, tablet, iWatch, keyboard, mouse, galaxy);

        List<Product> filteredProducts = new ProductFilter().filter(productList, new ProductTypeSpecification(ProductType.PHONE));
        filteredProducts.forEach(product -> System.out.println("Phones: " + product));

        List<Product> largeAndWhiteProducts = new ProductFilter().filter(productList, new SizeAndColorSpec(new SizeSpecification(Size.LARGE), new ColorSpecification(Color.BLACK)));

        largeAndWhiteProducts.forEach(product -> System.out.println("Large & White Product = " + product));
    }
}
