package com.springwell;

import com.springwell.ioc.bean.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

@SpringBootApplication
public class SpringBootJpaApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SpringBootJpaApplication(@Qualifier("RESTSearch") Search search) {
        this.search = search;
    }

    private final Search search;
    private Employee employee;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaApplication.class, args);
    }

    @Override
    public void run(String... args) {
        logger.info("Inside run method");
        String search = this.search.search();
        System.out.println(search);

        System.out.println("employee.getFirstName() = " + employee.getFirstName());
    }

    @Autowired
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

interface Search {

    String search();
}

@Service
class LinearSearch implements Search {

    @Override
    public String search() {
        return "Linear Search";
    }
}

@Service
class BinarySearch implements Search {

    @Override
    public String search() {
        return "Binary Search";
    }
}

@Service
class RESTSearch implements Search {
    @Override
    public String search() {
        return "REST Search";
    }
}