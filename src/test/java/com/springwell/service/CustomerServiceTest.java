package com.springwell.service;

import com.google.common.collect.Iterables;
import com.springwell.SpringBootJpaApplication;
import com.springwell.entity.Customer;
import com.springwell.repo.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootJpaApplication.class)
class CustomerServiceTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {

    }

    @Test
    void findById_SUCCESS() {
        Optional<Customer> optionalCustomer = customerRepository.findById(10001L);
        optionalCustomer.ifPresent(customer -> logger.info(String.valueOf(customer)));
        assertThat(optionalCustomer, equalTo(Optional.of(new Customer(10001L, "Adarshpal", "Brar", "adarshpal.brar@gmail.com"))));
        assertTrue(optionalCustomer.isPresent());
    }

    @Test
    void findById_FAIL() {
        Optional<Customer> optionalCustomer = customerRepository.findById(1001L);
        optionalCustomer.ifPresent(customer -> logger.info(String.valueOf(customer)));
        assertFalse(optionalCustomer.isPresent());
        assertThat(optionalCustomer, equalTo(Optional.empty()));
    }

    @Test
    void findAll_SUCCESS() {
        List<Customer> customers = customerRepository.findAll();
        assertThat(Iterables.size(customers), equalTo(4));
        assertTrue(Iterables.contains(customers, new Customer(10001L, "Adarshpal", "Brar", "adarshpal.brar@gmail.com")));
    }

    @Test
    void testDelete_SUCESSS() {
        customerRepository.deleteById(10001L);
        Iterable<Customer> customers = customerRepository.findAll();
        assertFalse(Iterables.contains(customers, new Customer(10001L, "Adarshpal", "Brar", "adarshpal.brar@gmail.com")));
    }

    @Test
    void testSorting_findAll() {
        List<Customer> customers = customerRepository.findAll(Sort.by(Sort.Direction.ASC, "email"));
        logger.info(String.valueOf(customers));
    }

    @Test
    void testFindByName() {
        List<Customer> customers = customerRepository.findByLastName("Brar");
        assertThat(customers.size(), equalTo(3));
    }

    @Test
    void testCaching() {
        Optional<Customer> optionalCustomer1 = customerRepository.findById(10001L);

        logger.error("Logging === Logging === Logging === Logging === Logging === ");

        Optional<Customer> optionalCustomer2 = customerRepository.findById(10001L);

        optionalCustomer1.ifPresent(customer -> logger.info("Customer 1" + customer));
        optionalCustomer2.ifPresent(customer -> logger.info("Customer 1" + customer));
    }
}