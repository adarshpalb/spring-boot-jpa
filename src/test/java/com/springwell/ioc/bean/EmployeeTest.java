package com.springwell.ioc.bean;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@EnableConfigurationProperties(value = Employee.class)
@TestPropertySource("classpath:props/SportsProperties.properties")
public class EmployeeTest {

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private Employee employee;

    @Test
    public void givenUserDefinedPOJO_whenBindingPropertiesFile_thenAllFieldsAreSet() {

        assertThat("adarshpal.brar@gmail.com", equalTo(employee.getEmail()));
    }
}